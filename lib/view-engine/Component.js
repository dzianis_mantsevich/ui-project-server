let fs = require('fs');
let _ = require('lodash');
let config = require('./../../config/default');
let {resolvePath, resolveLocalPath} = require('./../helpers');
let prerender = require('./prerender-hooks');
let doT = require('dot/doT.js');

class VirtualContext {
}

class Component {

  constructor(alias, options = {}, context = new VirtualContext()) {
    this.config = {};
    this.data = {};
    this.name = alias;
    this.options = options;
    this.context = context;
    this.error = null;
    this.componentPath = resolvePath(`${config.components}/${alias}`);

    return this._init() || this;
  }

  _init() {
    try {

      if (!fs.existsSync(this.componentPath)) {
        throw new Error(`Component "${this.name}" doesn't exist.`);
      }

      let configJSON = this.getConfig();
      this.config = configJSON;

      let {options: {data}} = this;

      // Eval data
      if (typeof data === 'string') {
        if (configJSON.data[data]) {
          this.data = configJSON.data[data]
        } else {
          throw new Error(`Invalid data(value: "${data}") option was provided into the component. Possible values: ${Object.keys(configJSON.data).join(', ') || 'no'}`);
        }
      } else if (typeof data === 'object') {
        this.data = data;
      } else {
        this.data = configJSON.data;
      }

      this.view = this._getFile(configJSON.view);


      if (configJSON.controller) {
        let controller = _.extend(this, require(this.resolvePath(configJSON.controller)));
        controller.init && (controller.init());
        return controller;
      }

    } catch (e) {
      this.onError(`Can't init component: "${this.name}". Error: ${e.message}.`, e);
    }
  }

  resolvePath(file) {
    return `${this.componentPath}/${file}`;
  }

  _getFile(file) {
    try {
      return fs.readFileSync(this.resolvePath(file), 'utf8');
    } catch (e) {
      this.onError(`Can't read file: ${file}. Error: ${e.message}`, e);
    }
  }

  onError(msg, error = new Error()) {
    error.message = msg;
    this.error = this.error || error;
  }

  getConfig(configFileName = 'config.json') {
    try {
      let configFile = prerender
        .process(this._getFile(configFileName), {file: this.resolvePath(configFileName), options: {}});

      configFile = JSON.parse(configFile.replace(/\\/gi, '/')); // Hello Bill Gates!
      configFile.data = configFile.data || {};

      if (configFile['@extendConfig']) {
        configFile = Object.assign(this.getConfig(configFile['@extendConfig']), configFile);
      }
      return configFile;
    } catch (e) {
      this.onError(`Can't parse config file: ${configFileName}. Error: ${e.message}`, e);
    }
  }


  // Rendition methods
  initComponent(alias, options) {
    return new this.constructor(alias, options, this);
  }

  renderComponent(alias, options) {
    let component = this.initComponent(alias, options);
    return component.render();
  }

  render() {
    if (this.error) {
      return this.renderError(this.error);
    }
    let result = this._compileView();
    return this.error ? this.renderError(this.error) : result;
  }

  _compileView(view = this.view, data = this.data, fileName = this.resolvePath(this.config.view)) {
    try {
      let tplsConfig = config.doT;
      tplsConfig.varname = `${this.config.var || 'component'}, data, options, page, partial`;
      return doT.template(view, tplsConfig)(this, data, this.options, this._page, this.partial.bind(this));
    } catch (e) {
      this.onError(`Can't compile template: "${fileName}". Error: ${e.message}`, e);
    }
  }

  partial(partialPath, data) {
    try {
      return this._compileView(this._getFile(partialPath), data, this.resolvePath(partialPath));
    } catch (e) {
      this.onError(`Can't compile partial: "${partialPath}". Error: ${e.message}`, e);
    }
  };

  get _page() {
    let context = this.context;
    while (context && !context.location) context = context.context;
    return context || null;
  }

  renderError(error) {
    try {
      const standartErrorView = resolveLocalPath('views/partials/invalid-component.html');
      return config.invalidComponent ?
        this.renderComponent(config.invalidComponent, {data: error}) :
        this._compileView(fs.readFileSync(standartErrorView, 'utf-8'), error, standartErrorView);
    } catch (e) {
      return error.message;
    }
  }
}

module.exports = Component;
