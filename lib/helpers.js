let template = require('lodash/template');
let path = require('path');
let config = require('./../config/default');
let fs = require('fs');

exports.resolvePath = (file) => path.resolve(`${process.cwd()}/${file}`);

exports.serverRoot = exports.resolvePath(config.serverRoot);

exports.resolveServerPath = (file) => path.resolve(`${exports.serverRoot}/${file}`);

exports.resolveLocalPath = (file) => path.resolve(`${__dirname}/../`, file);

exports.template = (tpl) => template(tpl)(config);

exports.findView = (view) => {
  view = exports.findFileInFolders(view, config.views);
  if (view) {
    return view.replace(/\.html$/, '');
  }
};

exports.findFileInFolders = (file, folders = []) => {

  let fullPath = null;
  folders.some((folder) => {
    if (fs.existsSync(`${folder}/${file}`)) {
      fullPath = `${folder}/${file}`;
      return true;
    }
  });

  return fullPath;
};

exports.renderPage = (request, response, view, data = {}) => {
  let Page = require(config.pageClass);
  let page = new Page({ request, response, view, data });
  response.render(view.replace(/\.html$/, ''), page);
};
