let express = require('express');
let config = require('./../../config/default');
let {resolveServerPath} = require('./../helpers');

module.exports = (app) => {

  config.public && config.public.forEach((folder) => {
    folder = resolveServerPath(folder);
    if (folder.indexOf('static') !== -1) { // cache static folder
      app.use(express.static(folder, {maxAge: 3600000})); // 1h cache
    } else {
      app.use(express.static(folder));
    }
  });

};
