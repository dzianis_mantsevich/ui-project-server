// TODO: Rewrite!
let {findView, renderPage} = require('./../helpers');
module.exports = (app) => {

  // error handler
  app.use((err, request, response, next) => {
    //let Page = require(config.pageClass ? resolveServerPath(config.pageClass) : './../Page');

    response.locals.message = err.message;
    response.locals.error = err;

    // render the error page
    response.status(err.status || 500);
    let customErrorRendition = findView(`${err.status}.html`);
    renderPage(request, response, customErrorRendition || 'error', err); // Render Error page :)
  });

};
