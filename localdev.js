let merge = require('lodash/merge');
let defaultConfig = require('./config/default');
let {resolveServerPath, resolveLocalPath} = require('./lib/helpers');

exports.start = (serverConfig = {}) => {
  merge(defaultConfig, serverConfig);

  defaultConfig.serverViews = resolveServerPath(defaultConfig.views);
  defaultConfig.localViews = resolveLocalPath('views/');
  defaultConfig.views = [
    defaultConfig.serverViews,
    defaultConfig.localViews
  ];

  defaultConfig.controllers = [
    resolveServerPath(defaultConfig.controllers),
    resolveLocalPath('./controllers')
  ];

  defaultConfig.baseRouter = defaultConfig.baseRouter ? resolveServerPath(defaultConfig.baseRouter) :
    resolveLocalPath('lib/routers/fsAsPages');

  defaultConfig.pageClass = defaultConfig.pageClass ? resolveServerPath(defaultConfig.pageClass) :
    resolveLocalPath('lib/view-engine/Page');

  defaultConfig.componentClass = defaultConfig.componentClass ? resolveServerPath(defaultConfig.componentClass) :
    resolveLocalPath('lib/view-engine/Component');

  global.SERVER_CONFIG = defaultConfig;

  require('./bin/www');
};
