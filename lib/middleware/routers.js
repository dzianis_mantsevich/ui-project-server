let config = require('./../../config/default');

module.exports = (app) => require(config.baseRouter)(app);
