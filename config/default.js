module.exports = {
  openAfterStart: 'http://localhost:<%= port %>/index.html',
  baseRouter: null, // relative path to base router
  port: 3000,
  get doT() {
    return {
      evaluate: /\{\{([\s\S]+?)\}\}/g,
      interpolate: /\{\{=([\s\S]+?)\}\}/g,
      encode: /\{\{!([\s\S]+?)\}\}/g,
      use: /\{\{#([\s\S]+?)\}\}/g,
      define: /\{\{##\s*([\w\.$]+)\s*(\:|=)([\s\S]+?)#\}\}/g,
      conditional: /\{\{\?(\?)?\s*([\s\S]*?)\s*\}\}/g,
      iterate: /\{\{~\s*(?:\}\}|([\s\S]+?)\s*\:\s*([\w$]+)\s*(?:\:\s*([\w$]+))?\s*\}\})/g,
      varname: 'layout, partial, locals, page',
      strip: false,
      append: true,
      selfcontained: false
    }
  },
  defaultPageUrl: 'index.html',
  defaultPageRendition: 'index.html',

  // custom folders
  serverRoot: 'server/',
  views: 'views/',
  controllers: 'controllers/',
  components: 'src/components/',
  middleware: 'middleware/*.js',
  public: [
    'static/',
    '../.temp/dest/'
  ]
};
