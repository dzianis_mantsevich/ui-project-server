let _ = require('lodash');
let path = require('path');
let config = require('./../config/default');
let glob = require('glob');
module.exports = function (folderPath) {
  if (folderPath.endsWith('.html')) {
    folderPath = path.dirname(folderPath);
    if (!folderPath.endsWith('/')) folderPath += '/';
  }
  return glob.sync('*', {
    cwd: `${config.serverViews}/pages/${folderPath}`,
    mark: true
  }).map((file) => {
    if (file.startsWith('_')) return null;
    let isDir = file.endsWith('/');
    return {
      title: _.startCase(file.replace(/\.html$/, '')),
      isDir: isDir,
      href: isDir ? `${folderPath}${file}${config.defaultPageUrl}` : `${folderPath}${file}`
    };
  }).filter(v => !!v);
};
