let _ = require('lodash');
let path = require('path');
let config = require('./../config/default');

module.exports = function () {
  if (!this._breadcrumbs) {

    let href = '/';
    let urlPathname = this.location.pathname;
    let breadCrumbs = urlPathname.split('/').map((urlPart) => {
      if (urlPart) {
        let htmlPage = urlPart.endsWith('.html') ? urlPart : config.defaultPageUrl;

        if (!urlPart.endsWith('.html')) {
          href += `${urlPart}/`;
        } else if (htmlPage === config.defaultPageUrl) {
          return null;
        }

        return {
          title: _.startCase(urlPart.replace(/\.html$/, '')),
          href: href + htmlPage
        };

      } else {
        return {
          title: 'Home',
          href: '/'
        };
      }
    }).filter(v => !!v);

    this._breadcrumbs = breadCrumbs;

  }
};
