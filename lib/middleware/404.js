// TODO: Rewrite! Add rendition for 404 page
module.exports = (app) => {

  // catch 404 and forward to error handler
  app.use((req, res, next) => {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

};
