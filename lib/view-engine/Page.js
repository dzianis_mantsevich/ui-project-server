let url = require('url');
let fs = require('fs');
let path = require('path');
let _ = require('lodash');
let config = require('./../../config/default');
let {findFileInFolders, resolveServerPath} = require('./../helpers');

// Currently doT.js express view engine doesn't support class instance
function Page(options) {
  this.request = options.request;
  this.response = options.response;
  this.view = options.view;
  this.data = options.data || {};
  this.location = url.parse(this.request.url, true);

  !this.view.endsWith('.html') && (this.view += '.html');
}

Page.prototype = {

  getTemplate(filename, options, callback) {
    let isAsync = callback && typeof callback === 'function';
    !filename.endsWith('.html') && (filename += '.html');

    if (!isAsync) return processPageTemplate(fs.readFileSync(filename, 'utf8'), filename, options);

    // async
    fs.readFile(filename, 'utf8', (err, str) => {
      if (err) {
        callback(new Error(`Failed to open view file (${filename})`));
        return;
      }
      try {
        str = processPageTemplate(str, filename, options);
        callback(null, str);
      } catch (err) {
        callback(err);
      }
    });
  },

  // Helpers
  getPageTitle(layoutTitle) {
    if (layoutTitle) {
      return layoutTitle;
    } else {
      let url = this.location.pathname;
      if (url.endsWith(config.defaultPageUrl)) {
        url = path.dirname(url);
      } else {
        url = url.replace(/\.html$/, '');
      }
      return _.startCase(_.last(url.split('/')));
    }
  },

  getTitle(layoutTitle) {
    if (layoutTitle) {
      return layoutTitle;
    } else {
      let url = this.location.pathname;
      if (url.endsWith(config.defaultPageUrl)) {
        url = path.dirname(url);
      } else {
        url = url.replace(/\.html$/, '');
      }
      return _.compact(url.split('/').reverse().map(_.startCase)).join(' | ');
    }
  },

  resolveURL(url = '') {
    if (url.startsWith('/')) {
      return './' + path.relative(path.dirname(this.location.pathname), url);
    }
    return url;
  },

  invokeController(controllerName, ...args) {
    try {
      let controller = findFileInFolders(`${controllerName}.js`, config.controllers);
      if (!controller) {
        throw new Error(`Can't find files: ${config.controllers.map((folder) => folder + '/' + controllerName + '.js')}`);
      }
      controller = require(controller);
      if (typeof controller === 'function') return controller.apply(this, args) || '';
      return '';
    } catch (e) {
      throw new Error(`Problems with "${controllerName}" controller. Reason: ${e.message}`);
    }
  },

  getFile(file) {
    return fs.readFileSync(resolveServerPath(file));
  },

  initComponent(component, options) {
    let Component = require(config.componentClass);
    return new Component(component, options, this);
  },

  renderComponent(component, options) {
    return this.initComponent(component, options).render();
  }

};

module.exports = Page;

function processPageTemplate(pageTemplate, file, options) {
  return require('./prerender-hooks').process(pageTemplate, {file, options});
}

